// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bufio"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base32"
	"encoding/binary"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	s := bufio.NewScanner(os.Stdin)
	s.Scan()
	secret := strings.ToUpper(strings.ReplaceAll(s.Text(), " ", ""))
	secretBytes, err := base32.StdEncoding.DecodeString(secret)
	if err != nil {
		fmt.Fprintln(os.Stderr, "bad secret")
		os.Exit(1)
	}
	h := hmac.New(sha1.New, secretBytes)
	counter := time.Now().Unix() / 30
	_ = binary.Write(h, binary.BigEndian, counter)
	sum := h.Sum(nil)
	offset := sum[len(sum)-1] & 0xf
	value := binary.BigEndian.Uint32(sum[offset:]) & 0x7f_ff_ff_ff
	fmt.Printf("%06d\n", value%1e6)
}
